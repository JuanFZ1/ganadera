<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


use app\models\Finca;
use app\models\Propietario;
use app\models\Criador;
use app\models\Animal;

use app\models\Vacunacion;
use app\models\Veterinario;
use app\models\User;
use app\models\ProduccionLactea;




//use app\models\ValidateSafePassword;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
{
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $fincas = Finca::find()->asArray()->count();        
        $propietarios = Propietario::find()->asArray()->count();
        $criadores = Criador::find()->asArray()->count();
        $animales = Animal::find()->asArray()->count();


        $vacunaciones = Vacunacion::find()->asArray()->count();        
        $veterinarios = Veterinario::find()->asArray()->count();
        $usuarios = User::find()->asArray()->count();
        $produccionlactea = ProduccionLactea::find()->asArray()->count();


        //return $this->render('index');
        return $this->render('index', [
            'fincas' => $fincas,
            'propietarios' => $propietarios,
            'criadores' => $criadores,
            'animales' => $animales,
            'vacunaciones' => $vacunaciones,
            'veterinarios' => $veterinarios,
            'usuarios' => $usuarios,
            'produccionlactea' => $produccionlactea,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $_SESSION['perfil'] = null;
        $_SESSION['user_type_id'] = null;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            //var_dump(Yii::$app->user->identity->user_type_id);die();

            /*
            $model->username = trim(strtolower(@$_POST['LoginForm']['username']));
            $model->username = $model->username;
            $model->password = trim(@$_POST['LoginForm']['password']);
            $model->password = ValidateSafePassword::Hash($model->password);
            */
            //VARIABLE DE SESSION
            $_SESSION['user_type_id'] = Yii::$app->user->identity->user_type_id;

            return $this->goHome();
        }
        $_SESSION['user_type_id'] = 0;

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
        

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $_SESSION['user_type_id'] = 0;

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
