<?php

namespace app\controllers;

use app\models\Animal;
use app\models\AnimalSearch;

use app\models\Finca;
use app\models\FincaSearch;
use app\models\Criador;
use app\models\CriadorSearch;
use app\models\GrupoFisiologico;
use app\models\GrupoFisiologicoSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * AnimalController implements the CRUD actions for Animal model.
 */
class AnimalController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                /*
                'access'=>[
                    'class'=> AccessControl::className(),
                    'rules'=> [
                        [
                            'allow'=>true,
                            'roles'=>['@']
                        ]
                    ]
                ]
                ,*/
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Animal models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AnimalSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Animal model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);
        $finca = Finca::findOne($model->id_finca);
        $criador = Criador::findOne($model->id_criadores);
        $grupo_fisiologico = GrupoFisiologico::findOne($model->id_grupo_fisiologico);

        //var_dump($propietario); die();

        return $this->render('view', [
            'model' => $model,
            'finca' => $finca,
            'criador' => $criador,
            'grupo_fisiologico' => $grupo_fisiologico
        ]);
    }

    /**
     * Creates a new Animal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Animal();
        $modelFincas = Finca::find()->asArray()->all();
        $modelCriadores = Criador::find()->asArray()->all();
        $modelGrupoFisiologicos = GrupoFisiologico::find()->asArray()->all();

        if ($this->request->isPost) {
            $model->created_at = date('Y-m-d');
            //$model->updated_at = date('Y-m-d');
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
            'modelFincas' => $modelFincas,
            'modelCriadores' => $modelCriadores,
            'modelGrupoFisiologicos' => $modelGrupoFisiologicos,
        ]);
    }

    /**
     * Updates an existing Animal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelFincas = Finca::find()->asArray()->all();
        $modelCriadores = Criador::find()->asArray()->all();
        $modelGrupoFisiologicos = GrupoFisiologico::find()->asArray()->all();
        //var_dump($modelGrupoFisiologicos);die();

        //$model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'modelFincas' => $modelFincas,
            'modelCriadores' => $modelCriadores,
            'modelGrupoFisiologicos' => $modelGrupoFisiologicos
        ]);
    }

    /**
     * Deletes an existing Animal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Animal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Animal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Animal::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }





}
