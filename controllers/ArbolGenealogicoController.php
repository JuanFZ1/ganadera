<?php

namespace app\controllers;

use app\models\ArbolGenealogico;
use app\models\ArbolGenealogicoSearch;
use app\models\Animal;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * ArbolGenealogicoController implements the CRUD actions for ArbolGenealogico model.
 */
class ArbolgenealogicoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [/*
                'access'=>[
                    'class'=> AccessControl::className(),
                    'rules'=> [
                        [
                            'allow'=>true,
                            'roles'=>['@']
                        ]
                    ]
                ]
                ,
                */
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ArbolGenealogico models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ArbolGenealogicoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ArbolGenealogico model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
            $model = $this->findModel($id);

            $modelanimal = Animal::findOne($model->id_animal);
            $modelpadre = Animal::findOne($model->id_padre);
            $modelmadre = Animal::findOne($model->id_madre);
            $modelabuelo_paterno = Animal::findOne($model->id_abuelo_paterno);
            $modelabuela_paterna = Animal::findOne($model->id_abuela_paterna);
            $modelabuelo_materna = Animal::findOne($model->id_abuelo_materna);
            $modelabuela_materna = Animal::findOne($model->id_abuela_materna);

        return $this->render('view', [
            'model' => $model,
            'modelanimal' => $modelanimal,
            'modelpadre' => $modelpadre,
            'modelmadre' => $modelmadre,
            'modelabuelo_paterno' => $modelabuelo_paterno,
            'modelabuela_paterna' => $modelabuela_paterna,
            'modelabuelo_materna' => $modelabuelo_materna,
            'modelabuela_materna' => $modelabuela_materna,
        ]);
    }

    /**
     * Creates a new ArbolGenealogico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ArbolGenealogico();
        $modelanimal = Animal::find()->asArray()->all();
        $modelpadre = Animal::find()->asArray()->all();
        $modelmadre = Animal::find()->asArray()->all();
        $modelabuelo_paterno = Animal::find()->asArray()->all();
        $modelabuela_paterna = Animal::find()->asArray()->all();
        $modelabuelo_materna = Animal::find()->asArray()->all();
        $modelabuela_materna = Animal::find()->asArray()->all();

        if ($this->request->isPost) {
            $model->created_at = date('Y-m-d');
            //$model->updated_at = date('Y-m-d');
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
            'modelanimal' => $modelanimal,
            'modelpadre' => $modelpadre,
            'modelmadre' => $modelmadre,
            'modelabuelo_paterno' => $modelabuelo_paterno,
            'modelabuela_paterna' => $modelabuela_paterna,
            'modelabuelo_materna' => $modelabuelo_materna,
            'modelabuela_materna' => $modelabuela_materna,
        ]);
    }

    /**
     * Updates an existing ArbolGenealogico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelanimal = Animal::find()->asArray()->all();
        $modelpadre = Animal::find()->asArray()->all();
        $modelmadre = Animal::find()->asArray()->all();
        $modelabuelo_paterno = Animal::find()->asArray()->all();
        $modelabuela_paterna = Animal::find()->asArray()->all();
        $modelabuelo_materna = Animal::find()->asArray()->all();
        $modelabuela_materna = Animal::find()->asArray()->all();

        //$model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'modelanimal' => $modelanimal,
            'modelpadre' => $modelpadre,
            'modelmadre' => $modelmadre,
            'modelabuelo_paterno' => $modelabuelo_paterno,
            'modelabuela_paterna' => $modelabuela_paterna,
            'modelabuelo_materna' => $modelabuelo_materna,
            'modelabuela_materna' => $modelabuela_materna,
        ]);
    }

    /**
     * Deletes an existing ArbolGenealogico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArbolGenealogico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ArbolGenealogico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArbolGenealogico::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
