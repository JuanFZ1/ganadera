<?php

namespace app\controllers;

use app\models\RevisionGinecologica;
use app\models\RevisionGinecologicaSearch;
use app\models\Animal;
use app\models\Veterinario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * RevisionGinecologicaController implements the CRUD actions for RevisionGinecologica model.
 */
class RevisionginecologicaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                /*
                'access'=>[
                    'class'=> AccessControl::className(),
                    'rules'=> [
                        [
                            'allow'=>true,
                            'roles'=>['@']
                        ]
                    ]
                ]
                ,
                */
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all RevisionGinecologica models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RevisionGinecologicaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RevisionGinecologica model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
            $model = $this->findModel($id);
            $animal = Animal::findOne($model->id_animal);
            $veterinario = Veterinario::findOne($model->id_veterinario);

        return $this->render('view', [
            'model' => $model,
            'animal' => $animal,
            'veterinario' => $veterinario,
        ]);
    }

    /**
     * Creates a new RevisionGinecologica model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new RevisionGinecologica();
        $modelAnimal = Animal::find()->asArray()->all();
        $modelVeterinario = Veterinario::find()->asArray()->all();

        if ($this->request->isPost) {
            $model->created_at = date('Y-m-d');
            //$model->updated_at = date('Y-m-d');
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelAnimal' => $modelAnimal,
            'modelVeterinario' => $modelVeterinario,
        ]);
    }

    /**
     * Updates an existing RevisionGinecologica model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAnimal = Animal::find()->asArray()->all();
        $modelVeterinario = Veterinario::find()->asArray()->all();

        //$model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'modelAnimal' => $modelAnimal,
            'modelVeterinario' => $modelVeterinario,
        ]);
    }

    /**
     * Deletes an existing RevisionGinecologica model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RevisionGinecologica model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return RevisionGinecologica the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RevisionGinecologica::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
