<?php

namespace app\controllers;

use app\models\User;
use app\models\Veterinario;
use app\models\Propietario;

use app\models\UserSearch;
use app\models\UserType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access'=>[
                    'class'=> AccessControl::className(),
                    'rules'=> [
                        [
                            'allow'=>true,
                            'roles'=>['@']
                        ]
                    ]
                ]
                ,
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $tipousuario = UserType::findOne($model->user_type_id);

        return $this->render('view', [
            'model' => $model,
            'tipousuario' => $tipousuario,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User();
        $modelVet = new Veterinario();
        $modelPro = new Propietario();

        $modelUserType = UserType::find()->asArray()->all();

        if ($this->request->isPost) {
            //Usuario
            $model->created_at = date('Y-m-d');
            $model->updated_at = date('Y-m-d');

            $model->load($this->request->post());

            $encriptar = crypt($_POST['User']['password'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
            $model['password']= $encriptar;

            
            // se consulta en la tabla de 
            //veterinarios
            if($model['user_type_id'] == 4){
                $modelVet->codigo_reg = '000' ;
                $modelVet->nombre = $model['nombre'];
                $modelVet->apellido = $model['apellido'];
                $modelVet->cedula = $model['ci'];
                $modelVet->telefono = '000';
                $modelVet->correo = $model['email'];
                $modelVet->created_at = date('Y-m-d');
                $modelVet->updated_at = date('Y-m-d');
                $modelVet->save();
            }
            //propietario
            if($model['user_type_id'] == 6){
                $modelPro->nombre = $model['nombre'];
                $modelPro->apellido = $model['apellido'];
                $modelPro->cedula = $model['ci'];
                $modelPro->correo = $model['email'];
                $modelPro->created_at = date('Y-m-d');
                $modelPro->updated_at = date('Y-m-d');
                $modelPro->save();
            }

            $model->save(); // guardar usuario

            return $this->redirect(['view', 'id' => $model->id]);
            /*
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            */
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
            'modelUserType' => $modelUserType,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUserType = UserType::find()->asArray()->all();
        //$model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');
        if ($this->request->isPost){

            $model->load($this->request->post());
            $encriptar = crypt($_POST['User']['password'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
            $model['password']= $encriptar;
            //$model['password']= '';
            // se consulta en las tablas

            if($model['user_type_id'] == 4){ //VETERINARIO
                $modelVet = Veterinario::findOne(['cedula' => $model['ci']]);
                if(!isset($modelVet) || $modelVet == null){
                    // crea el veterinario
                    $modelVet = new Veterinario();
                    //$modelVet->codigo_reg = '000' ;
                    $modelVet->nombre = $model['nombre'];
                    $modelVet->apellido = $model['apellido'];
                    $modelVet->cedula = $model['ci'];
                    $modelVet->telefono = '000';
                    $modelVet->correo = $model['email'];
                    $modelVet->created_at = date('Y-m-d');
                    $modelVet->updated_at = date('Y-m-d');
                }else{
                    //Actualiza el Veterinario
                    //$modelVet->codigo_reg = '000' ;
                    $modelVet->nombre = $model['nombre'];
                    $modelVet->apellido = $model['apellido'];
                    $modelVet->cedula = $model['ci'];
                    //$modelVet->telefono = '000';
                    $modelVet->correo = $model['email'];
                    //$modelVet->created_at = date('Y-m-d');
                    $modelVet->updated_at = date('Y-m-d');
                }
                //debe consultar en tabla de propietarios a ver si existe, para eliminarlo
                $modelPro = Propietario::findOne(['cedula' => $model['ci']]);
                if(isset($modelPro)){
                    $modelPro->delete();
                }

                $modelVet->save();
            }

            if($model['user_type_id'] == 6){//PROPIETARIO
                $modelPro = Propietario::findOne(['cedula' => $model['ci']]);
                if(!isset($modelPro) || $modelPro == null){ 
                    //Crea el Propietario
                    $modelPro = new Propietario();
                    $modelPro->nombre = $model['nombre'];
                    $modelPro->apellido = $model['apellido'];
                    $modelPro->cedula = $model['ci'];
                    $modelPro->telefono = '000';
                    $modelPro->correo = $model['email'];
                    $modelPro->created_at = date('Y-m-d');
                    $modelPro->updated_at = date('Y-m-d');
                }else{
                    //Actualiza el Propietario
                    $modelPro->nombre = $model['nombre'];
                    $modelPro->apellido = $model['apellido'];
                    $modelPro->cedula = $model['ci'];
                    $modelPro->correo = $model['email'];
                    //$modelPro->created_at = date('Y-m-d');
                    $modelPro->updated_at = date('Y-m-d');
                }

                //debe consultar en tabla de veterinario a ver si existe, para eliminarlo
                $modelVet = Veterinario::findOne(['cedula' => $model['ci']]);
                if(isset($modelVet)){
                    $modelVet->delete();
                }


               $modelPro->save();
            }
            

            // eliminar de proppietario o veterinario en caso de que cambie a otro tipo de usuario
            //FALTA REVISAR ESTO


            //actualizadno usuario
            $model->save(); 
            return $this->redirect(['view', 'id' => $model->id]);

        }
        /*
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        */
        return $this->render('update', [
            'model' => $model,
            'modelUserType' => $modelUserType,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $modelVet = new Veterinario();
        $modelPro = new Propietario();

        //$this->findModel($id)->delete(); //ORIGINAL
        $model = $this->findModel($id);
        //veterinarios
        if($model['user_type_id'] == 4){
            $modelVet = Veterinario::findOne(['cedula' => $model['ci']]);
            if(isset($modelVet)){
                $modelVet->delete();
            }
        }

        //propietario
        if($model['user_type_id'] == 6){
            $modelPro = Propietario::findOne(['cedula' => $model['ci']]);
            if(isset($modelPro)){
                $modelPro->delete();
            }
        }
        
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
