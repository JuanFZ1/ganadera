<?php

namespace app\controllers;

use app\models\User;
use app\models\Propietario;
use app\models\PropietarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * PropietarioController implements the CRUD actions for Propietario model.
 */
class PropietarioController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                /*
                'access'=>[
                    'class'=> AccessControl::className(),
                    'rules'=> [
                        [
                            'allow'=>true,
                            'roles'=>['@']
                        ]
                    ]
                ]
                ,
                */
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Propietario models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PropietarioSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Propietario model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Propietario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Propietario();

        if ($this->request->isPost) {
            $model->created_at = date('Y-m-d');
            //$model->updated_at = date('Y-m-d');
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Propietario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //var_dump($model['cedula']);die();
        //$model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            // consulta en la tabla de Usuario si existe para actualizar
            $modelUser = User::findOne(['ci' => $model['cedula']]);
            //var_dump($modelUser);die();
            if(isset($modelUser) || $modelUser != null){
                // Actualiza el Propietario
                $modelUser->nombre = $model['nombre'];
                $modelUser->apellido = $model['apellido'];
                $modelUser->ci = $model['cedula'];
                $modelUser->email = $model['correo'];
                $modelUser->updated_at = date('Y-m-d');
                $modelUser->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Propietario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Propietario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Propietario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Propietario::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
