<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_grupo_fisiologico".
 *
 * @property int $id
 * @property string $nombre
 * @property string $sexo
 * @property int $edad
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class GrupoFisiologico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_grupo_fisiologico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'sexo', 'edad'], 'required'],
            [['nombre', 'sexo'], 'string'],
            [['edad'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'sexo' => 'Sexo',
            'edad' => 'Edad',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
