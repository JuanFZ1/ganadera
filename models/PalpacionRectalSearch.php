<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PalpacionRectal;

/**
 * PalpacionRectalSearch represents the model behind the search form of `app\models\PalpacionRectal`.
 */
class PalpacionRectalSearch extends PalpacionRectal
{
    public $animal;
    public $veterinario;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_animal', 'id_veterinario'], 'integer'],
            [['observacion', 'fecha', 'created_at', 'updated_at', 
                'animal', 'veterinario'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PalpacionRectal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'observacion' => array(
                            'asc' => array('tbr_palpacion_rectal.observacion' => SORT_ASC, 'tbr_palpacion_rectal.observacion' => SORT_ASC),
                            'desc'=> array('tbr_palpacion_rectal.observacion' => SORT_DESC, 'tbr_palpacion_rectal.observacion' => SORT_DESC)
                    ),
                    'fecha' => array(
                            'asc' => array('tbr_palpacion_rectal.fecha' => SORT_ASC, 'tbr_palpacion_rectal.fecha' => SORT_ASC),
                            'desc'=> array('tbr_palpacion_rectal.fecha' => SORT_DESC, 'tbr_palpacion_rectal.fecha' => SORT_DESC)
                    ),
                    'animal' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'veterinario' => array(
                            'asc' => array('tbl_veterinarios.nombre' => SORT_ASC, 'tbl_veterinarios.nombre' => SORT_ASC),
                            'desc'=> array('tbl_veterinarios.nombre' => SORT_DESC, 'tbl_veterinarios.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('tbl_animales', 'id_animal = tbl_animales.id');
        $query->leftJoin('tbl_veterinarios', 'id_veterinario = tbl_veterinarios.id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'id_animal' => $this->id_animal,
            'id_veterinario' => $this->id_veterinario,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tbr_palpacion_rectal.observacion', $this->observacion])
            ->andFilterWhere(['like', 'tbr_palpacion_rectal.fecha', $this->fecha])
            ->andFilterWhere(['like', 'tbl_animales.nombre', $this->animal])
            ->andFilterWhere(['like', 'tbl_veterinarios.telefono', $this->veterinario]);

        return $dataProvider;
    }
}
