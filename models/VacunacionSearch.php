<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vacunacion;

/**
 * VacunacionSearch represents the model behind the search form of `app\models\Vacunacion`.
 */
class VacunacionSearch extends Vacunacion
{
    public $animal;
    public $veterinario;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_animal', 'id_veterinario'], 'integer'],
            [['nombre', 'principio_activo', 'marca', 'tipo', 'dosis', 'fecha', 'created_at', 'updated_at',
                'animal', 'veterinario'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacunacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'nombre' => array(
                            'asc' => array('tbr_vacunacion.nombre' => SORT_ASC, 'tbr_vacunacion.nombre' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.nombre' => SORT_DESC, 'tbr_vacunacion.nombre' => SORT_DESC)
                    ),
                    'principio_activo' => array(
                            'asc' => array('tbr_vacunacion.principio_activo' => SORT_ASC, 'tbr_vacunacion.principio_activo' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.principio_activo' => SORT_DESC, 'tbr_vacunacion.principio_activo' => SORT_DESC)
                    ),
                    'marca' => array(
                            'asc' => array('tbr_vacunacion.marca' => SORT_ASC, 'tbr_vacunacion.marca' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.marca' => SORT_DESC, 'tbr_vacunacion.marca' => SORT_DESC)
                    ),
                    'tipo' => array(
                            'asc' => array('tbr_vacunacion.tipo' => SORT_ASC, 'tbr_vacunacion.tipo' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.tipo' => SORT_DESC, 'tbr_vacunacion.tipo' => SORT_DESC)
                    ),
                    'dosis' => array(
                            'asc' => array('tbr_vacunacion.dosis' => SORT_ASC, 'tbr_vacunacion.dosis' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.dosis' => SORT_DESC, 'tbr_vacunacion.dosis' => SORT_DESC)
                    ),
                    'fecha' => array(
                            'asc' => array('tbr_vacunacion.fecha' => SORT_ASC, 'tbr_vacunacion.fecha' => SORT_ASC),
                            'desc'=> array('tbr_vacunacion.fecha' => SORT_DESC, 'tbr_vacunacion.fecha' => SORT_DESC)
                    ),
                    'animal' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'veterinario' => array(
                            'asc' => array('tbl_veterinarios.nombre' => SORT_ASC, 'tbl_veterinarios.nombre' => SORT_ASC),
                            'desc'=> array('tbl_veterinarios.nombre' => SORT_DESC, 'tbl_veterinarios.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'id_animal' => $this->id_animal,
            'id_veterinario' => $this->id_veterinario,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->leftJoin('tbl_animales', 'id_animal = tbl_animales.id');
        $query->leftJoin('tbl_veterinarios', 'id_veterinario = tbl_veterinarios.id');

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'principio_activo', $this->principio_activo])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'dosis', $this->dosis]);

        return $dataProvider;
    }
}
