<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProduccionLactea;

/**
 * ProduccionLacteaSearch represents the model behind the search form of `app\models\ProduccionLactea`.
 */
class ProduccionLacteaSearch extends ProduccionLactea
{
    public $animal;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'litros', 'id_animal'], 'integer'],
            [['codigo_reg', 'fecha_registro', 'created_at', 'updated_at',
                'animal'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProduccionLactea::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'codigo_reg' => array(
                            'asc' => array('tbr_produccion_lactea.codigo_reg' => SORT_ASC, 'tbr_produccion_lactea.codigo_reg' => SORT_ASC),
                            'desc'=> array('tbr_produccion_lactea.codigo_reg' => SORT_DESC, 'tbr_produccion_lactea.codigo_reg' => SORT_DESC)
                    ),
                    'litros' => array(
                            'asc' => array('tbr_produccion_lactea.litros' => SORT_ASC, 'tbr_produccion_lactea.litros' => SORT_ASC),
                            'desc'=> array('tbr_produccion_lactea.litros' => SORT_DESC, 'tbr_produccion_lactea.litros' => SORT_DESC)
                    ),
                    'fecha_registro' => array(
                            'asc' => array('tbr_produccion_lactea.fecha_registro' => SORT_ASC, 'tbr_produccion_lactea.fecha_registro' => SORT_ASC),
                            'desc'=> array('tbr_produccion_lactea.fecha_registro' => SORT_DESC, 'tbr_produccion_lactea.fecha_registro' => SORT_DESC)
                    ),
                    'animal' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('tbl_animales', 'id_animal = tbl_animales.id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'litros' => $this->litros,
            'fecha_registro' => $this->fecha_registro,
            'id_animal' => $this->id_animal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tbr_produccion_lactea.codigo_reg', $this->codigo_reg])
            ->andFilterWhere(['like', 'tbr_produccion_lactea.litros', $this->litros])
            ->andFilterWhere(['like', 'tbr_produccion_lactea.fecha_registro', $this->fecha_registro])

            ->andFilterWhere(['like', 'tbl_animales.nombre', $this->animal]);

        return $dataProvider;
    }
}
