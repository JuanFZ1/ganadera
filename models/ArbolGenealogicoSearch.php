<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ArbolGenealogico;

/**
 * ArbolGenealogicoSearch represents the model behind the search form of `app\models\ArbolGenealogico`.
 */
class ArbolGenealogicoSearch extends ArbolGenealogico
{
    public $animal;
    public $padre;
    public $madre;
    public $abuelo_paterno;
    public $abuela_paterna;
    public $abuelo_materna;
    public $abuela_materna;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_animal', 'id_padre', 'id_madre', 'id_abuelo_paterno', 'id_abuela_paterna', 'id_abuelo_materna', 'id_abuela_materna'], 'integer'],
            [['codigo_reg', 'created_at', 'updated_at',
                'animal', 'padre', 'madre', 'abuelo_paterno', 'abuela_paterna', 'abuelo_materna', 'abuela_materna'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArbolGenealogico::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'codigo_reg' => array(
                            'asc' => array('tbr_arbol_genealogico.codigo_reg' => SORT_ASC, 'tbr_arbol_genealogico.codigo_reg' => SORT_ASC),
                            'desc'=> array('tbr_arbol_genealogico.codigo_reg' => SORT_DESC, 'tbr_arbol_genealogico.codigo_reg' => SORT_DESC)
                    ),
                    'animal' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'padre' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'madre' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'abuelo_paterno' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'abuela_paterna' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'abuelo_materna' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'abuela_materna' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_animal' => $this->id_animal,
            'id_padre' => $this->id_padre,
            'id_madre' => $this->id_madre,
            'id_abuelo_paterno' => $this->id_abuelo_paterno,
            'id_abuela_paterna' => $this->id_abuela_paterna,
            'id_abuelo_materna' => $this->id_abuelo_materna,
            'id_abuela_materna' => $this->id_abuela_materna,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'codigo_reg', $this->codigo_reg]);

        return $dataProvider;
    }
}
