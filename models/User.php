<?php

namespace app\models;
use app\models\UserType;
use Yii;

/**
 * This is the model class for table "tbl_user".
 *
 * @property int $id
 * @property string $name
 * @property string $usuario
 * @property int $ci
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property int|null $user_type_id
 * @property string|null $remember_token
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    //public $user_type_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'name', 'username', 'ci', 'email', 'user_type_id', 'password'], 'required'],
            [['ci', 'user_type_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'apellido', 'name', 'username', 'email', 'email_verified_at', 'password'], 'string', 'max' => 255],
            [['remember_token'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'name' => 'Name',
            'username' => 'Usuario',
            //'perfil' => 'Perfil',
            'ci' => 'Cédula',
            'email' => 'Correo Electrónico',
            'email_verified_at' => 'Correo de Verificación',
            'password' => 'Contraseña',
            'user_type_id' => 'Tipo de Usuario',
            'remember_token' => 'Remember Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    
    public function validatePassword($password) {
        return \Yii::$app->getSecurity()->validatePassword($password,$this->password);
    }
    
    public static function findIdentity($id){
           return static::findOne(['id'=>$id]);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        return static::findOne(['password'=>$token]);
    }

    public function getAuthKey(){
        return($this->password);
    }

    public function getId(){
        return($this->getPrimaryKey());

    }

    public function validateAuthKey($authkey){
        return($this->password ===  $authkey);
    }

    //RELACIONES
    public function getListUsertype()
    {
        return $this->hasOne(UserType::className(), ['id' => 'user_type_id']);
    }

}
