<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RevisionGinecologica;

/**
 * RevisionGinecologicaSearch represents the model behind the search form of `app\models\RevisionGinecologica`.
 */
class RevisionGinecologicaSearch extends RevisionGinecologica
{
    public $animal;
    public $veterinario;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_animal', 'id_veterinario'], 'integer'],
            [['diagnostico', 'observacion', 'receta_medica', 'fecha', 'created_at', 'updated_at',
                'animal', 'veterinario'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RevisionGinecologica::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'diagnostico' => array(
                            'asc' => array('tbr_revision_ginecologica.diagnostico' => SORT_ASC, 'tbr_revision_ginecologica.diagnostico' => SORT_ASC),
                            'desc'=> array('tbl_fincas.diagnostico' => SORT_DESC, 'tbl_fincas.diagnostico' => SORT_DESC)
                    ),
                    'observacion' => array(
                            'asc' => array('tbr_revision_ginecologica.observacion' => SORT_ASC, 'tbr_revision_ginecologica.observacion' => SORT_ASC),
                            'desc'=> array('tbr_revision_ginecologica.observacion' => SORT_DESC, 'tbr_revision_ginecologica.observacion' => SORT_DESC)
                    ),
                    'receta_medica' => array(
                            'asc' => array('tbr_revision_ginecologica.receta_medica' => SORT_ASC, 'tbr_revision_ginecologica.receta_medica' => SORT_ASC),
                            'desc'=> array('tbr_revision_ginecologica.receta_medica' => SORT_DESC, 'tbr_revision_ginecologica.receta_medica' => SORT_DESC)
                    ),
                    'fecha' => array(
                            'asc' => array('tbr_revision_ginecologica.fecha' => SORT_ASC, 'tbr_revision_ginecologica.fecha' => SORT_ASC),
                            'desc'=> array('tbr_revision_ginecologica.fecha' => SORT_DESC, 'tbr_revision_ginecologica.fecha' => SORT_DESC)
                    ),
                    'animal' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'veterinario' => array(
                            'asc' => array('tbl_veterinarios.nombre' => SORT_ASC, 'tbl_veterinarios.nombre' => SORT_ASC),
                            'desc'=> array('tbl_veterinarios.nombre' => SORT_DESC, 'tbl_veterinarios.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'id_animal' => $this->id_animal,
            'id_veterinario' => $this->id_veterinario,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tbr_revision_ginecologica.diagnostico', $this->diagnostico])
            ->andFilterWhere(['like', 'tbr_revision_ginecologica.observacion', $this->observacion])
            ->andFilterWhere(['like', 'tbr_revision_ginecologica.receta_medica', $this->receta_medica])
            ->andFilterWhere(['like', 'tbr_revision_ginecologica.fecha', $this->fecha])

            ->andFilterWhere(['like', 'tbl_animales.nombre', $this->animal])
            ->andFilterWhere(['like', 'tbl_veterinarios.nombre', $this->veterinario]);

        return $dataProvider;
    }
}
