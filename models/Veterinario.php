<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_veterinarios".
 *
 * @property int $id
 * @property string $codigo_reg
 * @property string $nombre
 * @property string $apellido
 * @property int $cedula
 * @property string $telefono
 * @property string $correo
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Veterinario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_veterinarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_reg', 'nombre', 'apellido', 'cedula', 'correo'], 'required'],
            [['codigo_reg', 'nombre', 'apellido', 'correo'], 'string'],
            [['cedula'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['telefono'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_reg' => 'Código Reg',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'cedula' => 'Cédula',
            'telefono' => 'Teléfono',
            'correo' => 'Correo Electrónico',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
