<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_user_types".
 *
 * @property int $id
 * @property string $descripcion
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class UserType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
