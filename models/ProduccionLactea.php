<?php

namespace app\models;
use app\models\Animal;
use Yii;

/**
 * This is the model class for table "tbr_produccion_lactea".
 *
 * @property int $id
 * @property string $codigo_reg
 * @property int $litros
 * @property string $fecha_registro
 * @property int|null $id_animal
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ProduccionLactea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbr_produccion_lactea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_reg', 'litros', 'fecha_registro'], 'required'],
            [['codigo_reg'], 'string'],
            [['litros', 'id_animal'], 'integer'],
            [['fecha_registro', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function relations()
    {
        
        return array(
            'animal' => array(self::BELONGS_TO, 'Animal', 'id_animal'),
        );
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_reg' => 'Código Reg',
            'litros' => 'Litros',
            'fecha_registro' => 'Fecha Registro',
            //'id_animal' => 'Id Animal',
            'id_animal' => 'Animal',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    // RELACIONES
    public function getListAnimal()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_animal']);
    }


}
