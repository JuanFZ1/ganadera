<?php

namespace app\models;
use app\models\Finca;
use app\models\Criador;
use app\models\GrupoFisiologico;
use Yii;

/**
 * This is the model class for table "tbl_animales".
 *
 * @property int $id
 * @property string $codigo_reg
 * @property string $nombre
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property string $color
 * @property int $peso_nacer
 * @property int $peso_destete
 * @property int $peso_18_meses
 * @property int $peso_primer_parto
 * @property int|null $id_finca
 * @property int|null $id_criadores
 * @property int|null $id_grupo_fisiologico
 * @property int|null $estado
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Animal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_animales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_reg', 'nombre', 'sexo', 'fecha_nacimiento', 'color', 'peso_nacer', 'peso_destete', 'peso_18_meses', 'peso_primer_parto'], 'required'],
            [['peso_nacer', 'peso_destete', 'peso_18_meses', 'peso_primer_parto', 'id_finca', 'id_criadores', 'id_grupo_fisiologico', 'estado'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['codigo_reg', 'nombre', 'sexo', 'fecha_nacimiento', 'color'], 'string', 'max' => 255],
        ];
    }

    public function relations()
    {
        return array(
            'finca' => array(self::BELONGS_TO, 'Finca', 'id_finca'),
            'criador' => array(self::BELONGS_TO, 'Criador', 'id_criadores'),
            'grupo_fisiologico' => array(self::BELONGS_TO, 'GrupoFisiologico', 'id_grupo_fisiologico'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_reg' => 'Código Reg',
            'nombre' => 'Nombre',
            'sexo' => 'Sexo',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'color' => 'Color',
            'peso_nacer' => 'Peso Nacer',
            'peso_destete' => 'Peso Destete',
            'peso_18_meses' => 'Peso 18 Meses',
            'peso_primer_parto' => 'Peso Primer Parto',
            //'id_finca' => 'Id Finca',
            'id_finca' => 'Finca',
            //'id_criadores' => 'Id Criadores',
            'id_criadores' => 'Criador',
            //'id_grupo_fisiologico' => 'Id Grupo Fisiologico',
            'id_grupo_fisiologico' => 'Grupo Fisiológico',
            'estado' => 'Estado',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //RELACIONES
    public function getListFinca()
    {
        return $this->hasOne(Finca::className(), ['id' => 'id_finca']);
    }
    public function getListCriador()
    {
        return $this->hasOne(Criador::className(), ['id' => 'id_criadores']);
    }
    public function getListGrupoFisiologico()
    {
        return $this->hasOne(GrupoFisiologico::className(), ['id' => 'id_grupo_fisiologico']);
    }



}
