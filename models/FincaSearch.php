<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Finca;

/**
 * FincaSearch represents the model behind the search form of `app\models\Finca`.
 */
class FincaSearch extends Finca
{
    public $propietario;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'telefono', 'id_propietario'], 'integer'],
            [['codigo_reg', 'nombre', 'direccion', 'created_at', 'updated_at', 
                'propietario'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Finca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'codigo_reg' => array(
                            'asc' => array('tbl_fincas.codigo_reg' => SORT_ASC, 'tbl_fincas.codigo_reg' => SORT_ASC),
                            'desc'=> array('tbl_fincas.codigo_reg' => SORT_DESC, 'tbl_fincas.codigo_reg' => SORT_DESC)
                    ),
                    'nombre' => array(
                            'asc' => array('tbl_fincas.nombre' => SORT_ASC, 'tbl_fincas.nombre' => SORT_ASC),
                            'desc'=> array('tbl_fincas.nombre' => SORT_DESC, 'tbl_fincas.nombre' => SORT_DESC)
                    ),
                    'direccion' => array(
                            'asc' => array('tbl_fincas.direccion' => SORT_ASC, 'tbl_fincas.direccion' => SORT_ASC),
                            'desc'=> array('tbl_fincas.direccion' => SORT_DESC, 'tbl_fincas.direccion' => SORT_DESC)
                    ),
                    'telefono' => array(
                            'asc' => array('tbl_fincas.telefono' => SORT_ASC, 'tbl_fincas.telefono' => SORT_ASC),
                            'desc'=> array('tbl_fincas.telefono' => SORT_DESC, 'tbl_fincas.telefono' => SORT_DESC)
                    ),
                    'propietario' => array(
                            'asc' => array('tbl_propietarios.nombre' => SORT_ASC, 'tbl_propietarios.nombre' => SORT_ASC),
                            'desc'=> array('tbl_propietarios.nombre' => SORT_DESC, 'tbl_propietarios.nombre' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'telefono' => $this->telefono,
            'id_propietario' => $this->id_propietario,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->leftJoin('tbl_propietarios', 'id_propietario = tbl_propietarios.id');

        $query->andFilterWhere(['like', 'tbl_fincas.codigo_reg', $this->codigo_reg])
            ->andFilterWhere(['like', 'tbl_fincas.nombre', $this->nombre])
            ->andFilterWhere(['like', 'tbl_fincas.direccion', $this->direccion])
            ->andFilterWhere(['like', 'tbl_fincas.telefono', $this->telefono])

            ->andFilterWhere(['like', 'tbl_propietarios.nombre', $this->propietario]);

        return $dataProvider;
    }

}
