<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    public $tipousuario;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ci', 'user_type_id'], 'integer'],
            [['nombre', 'apellido', 'name', 'username', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at',
                'tipousuario'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => array(
                    'nombre' => array(
                            'asc' => array('tbl_user.nombre' => SORT_ASC, 'tbl_user.nombre' => SORT_ASC),
                            'desc'=> array('tbl_fincas.nombre' => SORT_DESC, 'tbl_fincas.nombre' => SORT_DESC)
                    ),
                    'apellido' => array(
                            'asc' => array('tbl_user.apellido' => SORT_ASC, 'tbl_user.apellido' => SORT_ASC),
                            'desc'=> array('tbl_fincas.apellido' => SORT_DESC, 'tbl_fincas.apellido' => SORT_DESC)
                    ),
                    'name' => array(
                            'asc' => array('tbl_user.name' => SORT_ASC, 'tbl_user.name' => SORT_ASC),
                            'desc'=> array('tbl_fincas.name' => SORT_DESC, 'tbl_fincas.name' => SORT_DESC)
                    ),
                    'username' => array(
                            'asc' => array('tbl_user.username' => SORT_ASC, 'tbl_user.username' => SORT_ASC),
                            'desc'=> array('tbl_user.username' => SORT_DESC, 'tbl_user.username' => SORT_DESC)
                    ),
                    'ci' => array(
                            'asc' => array('tbl_user.ci' => SORT_ASC, 'tbl_user.ci' => SORT_ASC),
                            'desc'=> array('tbl_user.ci' => SORT_DESC, 'tbl_user.ci' => SORT_DESC)
                    ),
                    'email' => array(
                            'asc' => array('tbl_user.email' => SORT_ASC, 'tbl_user.email' => SORT_ASC),
                            'desc'=> array('tbl_user.email' => SORT_DESC, 'tbl_user.email' => SORT_DESC)
                    ),
                    'tipousuario' => array(
                            'asc' => array('tbl_user_types.descripcion' => SORT_ASC, 'tbl_user_types.descripcion' => SORT_ASC),
                            'desc'=> array('tbl_user_types.descripcion' => SORT_DESC, 'tbl_user_types.descripcion' => SORT_DESC)
                    ),
                ),
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ci' => $this->ci,
            'user_type_id' => $this->user_type_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->leftJoin('tbl_user_types', 'user_type_id = tbl_user_types.id');

        $query->andFilterWhere(['like', 'tbl_user.nombre', $this->nombre])
            ->andFilterWhere(['like', 'tbl_user.apellido', $this->apellido])
            ->andFilterWhere(['like', 'tbl_user.name', $this->name])
            ->andFilterWhere(['like', 'tbl_user.username', $this->username])
            ->andFilterWhere(['like', 'tbl_user.email', $this->email])
            ->andFilterWhere(['like', 'tbl_user.email_verified_at', $this->email_verified_at])
            ->andFilterWhere(['like', 'tbl_user.password', $this->password])
            ->andFilterWhere(['like', 'tbl_user.remember_token', $this->remember_token])

            ->andFilterWhere(['like', 'tbl_user_types.descripcion', $this->tipousuario]);

        return $dataProvider;
    }
}
