<?php

namespace app\models;
use app\models\Propietario;
use Yii;

/**
 * This is the model class for table "tbl_fincas".
 *
 * @property int $id
 * @property string $codigo_reg
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 * @property int|null $id_propietario
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Finca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_fincas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_reg', 'nombre', 'direccion', 'telefono'], 'required'],
            [['codigo_reg', 'nombre', 'direccion'], 'string'],
            [['id_propietario'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['telefono'], 'string', 'max' => 11],
        ];
    }


    public function relations()
    {
        
        return array(
            'propietario' => array(self::BELONGS_TO, 'Propietario', 'id_propietario'),
        );
    }




    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_reg' => 'Código Reg',
            'nombre' => 'Nombre',
            'direccion' => 'Dirección',
            'telefono' => 'Telefono',
            //'id_propietario' => 'Id Propietario',
            'id_propietario' => 'Propietario',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    //RELACIONES
    public function getListPropietario()
    {
        return $this->hasOne(Propietario::className(), ['id' => 'id_propietario']);
    }

}
