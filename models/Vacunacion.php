<?php

namespace app\models;
use app\models\Animal;
use app\models\Veterinario;
use Yii;

/**
 * This is the model class for table "tbr_vacunacion".
 *
 * @property int $id
 * @property string $nombre
 * @property string $principio_activo
 * @property string $marca
 * @property string $tipo
 * @property string $dosis
 * @property string $fecha
 * @property int|null $id_animal
 * @property int|null $id_veterinario
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Vacunacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbr_vacunacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'principio_activo', 'marca', 'tipo', 'dosis', 'fecha'], 'required'],
            [['nombre', 'principio_activo', 'marca', 'tipo'], 'string'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['id_animal', 'id_veterinario'], 'integer'],
            [['dosis'], 'string', 'max' => 11],
        ];
    }

    public function relations()
    {
        
        return array(
            //'propietario' => array(self::HAS_MANY, 'Propietario', 'id_propietario'),
            'animal' => array(self::BELONGS_TO, 'Animal', 'id_animal'),
            'veterinario' => array(self::BELONGS_TO, 'Veterinario', 'id_veterinario'),
        );
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'principio_activo' => 'Principio Activo',
            'marca' => 'Marca',
            'tipo' => 'Tipo',
            'dosis' => 'Dosis',
            'fecha' => 'Fecha',
            //'id_animal' => 'Id Animal',
            'id_animal' => 'Animal',
            //'id_veterinario' => 'Id Veterinario',
            'id_veterinario' => 'Veterinario',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    // RELACIONES
    public function getListAnimal()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_animal']);
    }

    public function getListVeterinario()
    {
        return $this->hasOne(Veterinario::className(), ['id' => 'id_veterinario']);
    }
    
}
