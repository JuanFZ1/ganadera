<?php

namespace app\models;
use app\models\Animal;
use Yii;

/**
 * This is the model class for table "tbr_arbol_genealogico".
 *
 * @property int $id
 * @property string $codigo_reg
 * @property int|null $id_animal
 * @property int|null $id_padre
 * @property int|null $id_madre
 * @property int|null $id_abuelo_paterno
 * @property int|null $id_abuela_paterna
 * @property int|null $id_abuelo_materna
 * @property int|null $id_abuela_materna
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ArbolGenealogico extends \yii\db\ActiveRecord
{
    public $animal;
    public $padre;
    public $madre;
    public $abuelo_paterno;
    public $abuela_paterna;
    public $abuelo_materno;
    public $abuela_materna;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbr_arbol_genealogico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_reg'], 'required'],
            [['codigo_reg'], 'string'],
            [['id_animal', 'id_padre', 'id_madre', 'id_abuelo_paterno', 'id_abuela_paterna', 'id_abuelo_materna', 'id_abuela_materna'], 'integer'],
            [['created_at', 'updated_at',
                'animal', 'padre', 'madre', 'abuelo_paterno', 'abuela_paterna', 'abuelo_materno', 'abuela_materna' 
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_reg' => 'Código Reg',
            'id_animal' => 'Animal',
            'id_padre' => 'Padre',
            'id_madre' => 'Madre',
            'id_abuelo_paterno' => 'Abuelo Paterno',
            'id_abuela_paterna' => 'Abuela Paterna',
            'id_abuelo_materna' => 'Abuelo Materno',
            'id_abuela_materna' => 'Abuela Materna',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    //RELACIONES
    public function getListAnimal()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_animal']);
    }
    //PADRES
    public function getListPadre()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_padre']);
    }
    public function getListMadre()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_madre']);
    }
    //ABUELOS
    public function getListAbuelopaterno()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_abuelo_paterno']);
    }
    public function getListAbuelaPaterna()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_abuela_paterna']);
    }
    public function getListAbueloMaterno()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_abuelo_materna']);
    }
    public function getListAbuelaMaterna()
    {
        return $this->hasOne(Animal::className(), ['id' => 'id_abuela_materna']);
    }
}
