<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Animal;

/**
 * AnimalSearch represents the model behind the search form of `app\models\Animal`.
 */
class AnimalSearch extends Animal
{
    public $finca;
    public $criador;
    public $grupo_fisiologico;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'peso_nacer', 'peso_destete', 'peso_18_meses', 'peso_primer_parto', 'id_finca', 'id_criadores', 'id_grupo_fisiologico', 'estado'], 'integer'],
            [['codigo_reg', 'nombre', 'sexo', 'fecha_nacimiento', 'color', 'created_at', 'updated_at',
                'finca', 'criador', 'grupo_fisiologico'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Animal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort' => array(
                'attributes' => array(
                    'codigo_reg' => array(
                            'asc' => array('tbl_animales.codigo_reg' => SORT_ASC, 'tbl_animales.codigo_reg' => SORT_ASC),
                            'desc'=> array('tbl_animales.codigo_reg' => SORT_DESC, 'tbl_animales.codigo_reg' => SORT_DESC)
                    ),
                    'nombre' => array(
                            'asc' => array('tbl_animales.nombre' => SORT_ASC, 'tbl_animales.nombre' => SORT_ASC),
                            'desc'=> array('tbl_animales.nombre' => SORT_DESC, 'tbl_animales.nombre' => SORT_DESC)
                    ),
                    'sexo' => array(
                            'asc' => array('tbl_animales.sexo' => SORT_ASC, 'tbl_animales.sexo' => SORT_ASC),
                            'desc'=> array('tbl_animales.sexo' => SORT_DESC, 'tbl_animales.sexo' => SORT_DESC)
                    ),
                    'fecha_nacimiento' => array(
                            'asc' => array('tbl_animales.fecha_nacimiento' => SORT_ASC, 'tbl_animales.fecha_nacimiento' => SORT_ASC),
                            'desc'=> array('tbl_animales.fecha_nacimiento' => SORT_DESC, 'tbl_animales.fecha_nacimiento' => SORT_DESC)
                    ),
                    'color' => array(
                            'asc' => array('tbl_animales.color' => SORT_ASC, 'tbl_animales.color' => SORT_ASC),
                            'desc'=> array('tbl_animales.color' => SORT_DESC, 'tbl_animales.color' => SORT_DESC)
                    ),


                    'finca' => array(
                            'asc' => array('tbl_fincas.nombre' => SORT_ASC, 'tbl_fincas.nombre' => SORT_ASC),
                            'desc'=> array('tbl_fincas.nombre' => SORT_DESC, 'tbl_fincas.nombre' => SORT_DESC)
                    ),
                    'criador' => array(
                            'asc' => array('tbl_criadores.nombre' => SORT_ASC, 'tbl_criadores.nombre' => SORT_ASC),
                            'desc'=> array('tbl_criadores.nombre' => SORT_DESC, 'tbl_criadores.nombre' => SORT_DESC)
                    ),
                    'grupo_fisiologico' => array(
                            'asc' => array('tbl_grupo_fisiologico.nombre' => SORT_ASC, 'tbl_grupo_fisiologico.nombre' => SORT_ASC),
                            'desc'=> array('tbl_grupo_fisiologico.nombre' => SORT_DESC, 'tbl_grupo_fisiologico.nombre' => SORT_DESC)
                    ),
                    'estado' => array(
                            'asc' => array('tbl_animales.estado' => SORT_ASC, 'tbl_animales.estado' => SORT_ASC),
                            'desc'=> array('tbl_animales.estado' => SORT_DESC, 'tbl_animales.estado' => SORT_DESC)
                    ),
                ),
            ),

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'peso_nacer' => $this->peso_nacer,
            'peso_destete' => $this->peso_destete,
            'peso_18_meses' => $this->peso_18_meses,
            'peso_primer_parto' => $this->peso_primer_parto,
            'id_finca' => $this->id_finca,
            'id_criadores' => $this->id_criadores,
            'id_grupo_fisiologico' => $this->id_grupo_fisiologico,
            'estado' => $this->estado,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->leftJoin('tbl_fincas', 'id_finca = tbl_fincas.id');
        $query->leftJoin('tbl_criadores', 'id_criadores = tbl_criadores.id');
        $query->leftJoin('tbl_grupo_fisiologico', 'id_grupo_fisiologico = tbl_grupo_fisiologico.id');

        $query->andFilterWhere(['like', 'tbl_animales.codigo_reg', $this->codigo_reg])
            ->andFilterWhere(['like', 'tbl_animales.nombre', $this->nombre])
            ->andFilterWhere(['like', 'tbl_animales.sexo', $this->sexo])
            ->andFilterWhere(['like', 'tbl_animales.fecha_nacimiento', $this->fecha_nacimiento])
            ->andFilterWhere(['like', 'tbl_animales.color', $this->color])

            ->andFilterWhere(['like', 'tbl_fincas.nombre', $this->finca])
            ->andFilterWhere(['like', 'tbl_criadores.nombre', $this->criador])
            ->andFilterWhere(['like', 'tbl_grupo_fisiologico.nombre', $this->grupo_fisiologico]);

        return $dataProvider;
    }
}
