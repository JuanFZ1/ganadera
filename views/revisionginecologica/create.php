<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RevisionGinecologica $model */

$this->title = 'Crear Revision Ginecologica';
$this->params['breadcrumbs'][] = ['label' => 'Revisiones Ginecologicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revision-ginecologica-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal,
        'modelVeterinario' => $modelVeterinario,
    ]) ?>

</div>
