<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\RevisionGinecologicaSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="revision-ginecologica-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'diagnostico') ?>

    <?= $form->field($model, 'observacion') ?>

    <?= $form->field($model, 'receta_medica') ?>

    <?= $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'id_animal') ?>

    <?php // echo $form->field($model, 'id_veterinario') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
