<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\RevisionGinecologica $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="revision-ginecologica-form">

    <?php $form = ActiveForm::begin(); ?>
    <table>
        <tbody>
            <tr>
                <td>
                    <?php 
                    //$form->field($model, 'id_animal')->textInput() 
                    echo($form->field($model, 'id_animal')->dropDownList(ArrayHelper::map($modelAnimal, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                    ?>
                </td>
                <td>
                    <?php
                     //$form->field($model, 'id_veterinario')->textInput() 
                    echo($form->field($model, 'id_veterinario')->dropDownList(ArrayHelper::map($modelVeterinario, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
        <h3>Detalles de la Revisión</h3>
    <table class="table table-hover">
        <tbody>
            <tr>
                <td>
                    <?= $form->field($model, 'diagnostico')->textInput(['rows' => 1]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'observacion')->textInput(['rows' => 1]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'receta_medica')->textInput(['rows' => 1]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'fecha')->textInput() ?>
                </td>
            </tr>
        </tbody>
    </table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
