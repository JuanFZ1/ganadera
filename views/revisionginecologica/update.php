<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RevisionGinecologica $model */

$this->title = 'Actualizar Revision Ginecológica: ';
$this->params['breadcrumbs'][] = ['label' => 'Revisiones Ginecologicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="revision-ginecologica-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal,
        'modelVeterinario' => $modelVeterinario,
    ]) ?>

</div>
