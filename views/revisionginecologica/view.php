<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\RevisionGinecologica $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Revisiones Ginecologicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="revision-ginecologica-view">

    <h1>Revisión Ginecológica</h1>

    <h4>Animal</h4>
        <?= DetailView::widget([
        'model' => $animal,
        'attributes' => [
            'nombre',
        ],
    ]) ?>
    <h4>Veterinario</h4>
        <?= DetailView::widget([
        'model' => $veterinario,
        'attributes' => [
            'nombre',
            'apellido',
        ],
    ]) ?>

<h4>Detalles de la Revisión</h4>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'diagnostico:ntext',
            'observacion:ntext',
            'receta_medica:ntext',
            'fecha',
            //'id_animal',
            //'id_veterinario',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>
    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
