<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
//$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
//$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/iconoUCV.ico')]);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/3.ico')]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
    <?php
    NavBar::begin([
        //'brandLabel' => Html::img('../img/3.png'),
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar-expand-md navbar-primary bg-primary fixed-top']
    ]);


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [        
            /*
            Yii::$app->user->isGuest?['label' => 'Principal', 'url' => ['/site/index']]:'',
            Yii::$app->user->isGuest?['label' => 'Acerca', 'url' => ['/site/about']]:'',
            Yii::$app->user->isGuest?['label' => 'Contactos', 'url' => ['/site/contact']]:'',
            */
            ////modulos de usuario logueado

            !Yii::$app->user->isGuest ? ['label' => 'Fincas', 'url' => ['/finca']] : '',
            !Yii::$app->user->isGuest ? ['label' => 'Propietarios', 'url' => ['/propietario']] : '',
            !Yii::$app->user->isGuest ? ['label' => 'Criadores', 'url' => ['/criador']] : '',
            !Yii::$app->user->isGuest ? ['label' => 'Animales', 'url' => ['/animal']] : '',
            !Yii::$app->user->isGuest ? ['label' => 'Arbol Genealógico', 'url' => ['/arbolgenealogico']] : '',

            !Yii::$app->user->isGuest ? ['label' => 'Vacunaciones', 'url' => ['/vacunacion']] : '',
            !Yii::$app->user->isGuest ? ['label' => 'Veterinarios', 'url' => ['/veterinario']] : '',


            //Administrador
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1) ? ['label' => 'Tipos de Usuarios', 'url' => ['/usertype']] : '',
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1) ? ['label' => 'Usuarios', 'url' => ['/user']] : '',  

            //administrador y veterinario
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1 || $_SESSION["user_type_id"] == 4)  ? ['label' => 'Producción Láctea', 'url' => ['/produccionlactea']] : '',
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1 || $_SESSION["user_type_id"] == 4)  ? ['label' => 'Grupo Fisiológico', 'url' => ['/grupofisiologico']] : '',
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1 || $_SESSION["user_type_id"] == 4)  ? ['label' => 'Palpación Rectal', 'url' => ['/palpacionrectal']] : '',
            isset($_SESSION["user_type_id"]) && ($_SESSION["user_type_id"] == 1 || $_SESSION["user_type_id"] == 4)  ? ['label' => 'Revisión Ginecológica', 'url' => ['/revisionginecologica']] : '',

            Yii::$app->user->isGuest
                ? ['label' => 'Ingresar', 'url' => ['/site/login']]
                : '<li class="nav-item">'
                    . Html::beginForm(['/site/logout'])
                    . Html::submitButton(
                        'Salir (' . Yii::$app->user->identity->name . ')',
                        ['class' => 'nav-link btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
        ]
    ]);



    NavBar::end();
    ?>
</header>

<main id="main" class="flex-shrink-0" role="main">
    <div class="container">
        <?php if (!empty($this->params['breadcrumbs'])): ?>
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        <?php endif ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer id="footer" class="mt-auto py-3 bg-light">
    <div class="container">
        <div class="row text-muted">
            <div class="col-md-6 text-center text-md-start">&copy; Sistema Ganadera <?= date('Y') ?></div>
            <!-- <div class="col-md-6 text-center text-md-end"><?= Yii::powered() ?></div> -->
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
