<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\User $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= $form->field($model, 'ci')->textInput() ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'user_type_id')->textInput() 
                echo($form->field($model, 'user_type_id')->dropDownList(ArrayHelper::map($modelUserType, 'id', 'descripcion'), ['prompt' => 'Seleccione Uno' ]));
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= $form->field($model, 'email_verified_at')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                <?= //$form->field($model, 'password')->passwordInput(['maxlength' => true])
                    $form->field($model, 'password')->textInput(['maxlength' => true, 'type' => 'password']) 
                ?>
            </td>
        </tr>
    </tbody>
</table>



    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
