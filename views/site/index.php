<?php

/** @var yii\web\View $this */

$this->title = 'Sistema Ganadera - UCV';
?>
<div class="site-index">
<?php
    if(isset($_SESSION["user_type_id"]) && (
               $_SESSION["user_type_id"] == 1 // Administrador
            || $_SESSION["user_type_id"] == 2 // Coordinador
            || $_SESSION["user_type_id"] == 3 // Supervisor
            //|| $_SESSION["user_type_id"] == 4 // Veterinario
            //|| $_SESSION["user_type_id"] == 5 // Auxiliar
            || $_SESSION["user_type_id"] == 6 // propietario
        )){
    ?>
        <div class="jumbotron text-center bg-transparent mt-5 mb-5">
            <h4 class="display-4">
                <?php   
                        switch (Yii::$app->user->identity->user_type_id) {
                            case 0:
                                echo "Intruso";
                                break;
                            case 1:
                                echo "Administrador";
                                break;
                            case 2:
                                echo "Coordinador";
                                break;
                            case 3:
                                echo "Supervisor";
                                break;
                            case 4:
                                echo "Veterinario";
                                break;
                            case 5:
                                echo "Auxiliar";
                                break;
                            case 6:
                                echo "Propietario";
                                break;
                            default:
                                echo "Usuario";
                        }
                        echo(' '.Yii::$app->user->identity->name);  
                ?>
            </h4>
        </div>
        <div class="content">
            <section class="content">
              <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <?php
                      include "inicio/cajas-superiores.php";
                    ?>
                </div>
                <div class="row">
                    <?php
                      include "inicio/cajas-superiores2.php";
                    ?>
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
        </div>

     <div class="row">
        <div class="col-lg-12">
          <?php
          if($_SESSION["user_type_id"] == 1){
           //include "reportes/grafico-ventas.php";
           //include "reportes/grafico-ganado.php";
          }?>
        </div>
    </div>

    <?php
    }else {
    ?>
        <div class="jumbotron text-center bg-transparent mt-5 mb-5">
            <h1 class="display-4">Bienvenido a Ganadera System</h1>
            <img src="../img/3.png" width="200px">
            <p class="lead">Aplicación para el control de producción y ganadero.</p>
            <!-- <p><a class="btn btn-lg btn-success" href="index.php?r=site/login">Ingresar</a></p> -->
        </div>
        <div class="body-content">
            <section class="content">
                <div class="row">
                    <div class="col-lg-4 mb-3">
                        <h2>¿Que es el Sistema Ganadero?</h2>

                        <p>FREMOND (1967) conceptúa el sistema ganadero como una unidad de investigación que expresa una combinación en la que se interfieren elementos de orden natural, económico y sociológico. El dominio de un sistema en el interior de un área más o menos netamente delimitada permite definir una región ganadera.</p>

                        <p><a class="btn btn-outline-secondary" href="https://www.ingeba.org/lurralde/lurranet/lur19/19espej/19espejo.htm#:~:text=FREMOND%20(1967)%20concept%C3%BAa%20el%20sistema,permite%20definir%20una%20regi%C3%B3n%20ganadera." target="_blank">Ver Documentacion</a></p>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <h2>¿Cómo es el Manejo de la Ganadería?</h2>

                        <p>Se trata de un procedimiento mediante el cual se entra en contacto directo con el ganado para su posterior desplazamiento. Abarca desde su transporte hacia las distintas zonas donde puedan comer o dormir hasta la asistencia a los partos de las nuevas crías, pasando por la realización de distintos estudios sanitarios.</p>

                        <p><a class="btn btn-outline-secondary" href="https://www.yiiframework.com/forum/" target="_blank">Mas Informacion </a></p>
                    </div>
                    <div class="col-lg-4">
                        <h2>¿Qué hace el encargado o capataz de ganadería?</h2>

                        <p>Las personas que trabajan como ganaderos se encargan de cuidar el ganado de forma principal. Los animales con los que suelen trabajar son ganado vacuno, ovejas y cerdos. Durante su jornada laboral se encargan de la limpieza, ordeñar, alimentar y comprobar la salud de los animales a su cargo. ¿Te gustaría conocer más del encargado o capataz de ganadería?</p>

                        <p><a class="btn btn-outline-secondary" href="https://www.euroinnovaformacion.com.ve/profesion/encargado-o-capataz-de-ganaderia#:~:text=Y%20GESTI%C3%93N%20SOSTENIBLE-,%C2%BFQui%C3%A9n%20es%20el%20encargado%20o%20capataz%20de%20ganader%C3%ADa%3F,de%20los%20que%20se%20disponga." target="_blank">Ver más</a></p>
                    </div>
                    <div class="col-lg-4">
                        <h2>¿Cuál es el perfil profesional del encargado o capataz de ganadería?</h2>

                        <p>Un ganadero debe contar con una serie de características básicas que resultan fundamentales para llevar a cabo el trabajo indicado:
                            Capacidad de trabajar en equipo.
                            Disfrutar del trabajo al aire libre en el clima que sea.
                            Buena forma física para determinados trabajos.
                            Tener interés en la cría de los animales y sus cuidados.
                            Ser una persona responsable y dispuesto a tomar decisiones.
                            Habilidades en situaciones prácticas y capacidad de adaptación.</p>

                        <p><a class="btn btn-outline-secondary" href="https://www.euroinnovaformacion.com.ve/profesion/encargado-o-capataz-de-ganaderia#:~:text=Y%20GESTI%C3%93N%20SOSTENIBLE-,%C2%BFQui%C3%A9n%20es%20el%20encargado%20o%20capataz%20de%20ganader%C3%ADa%3F,de%20los%20que%20se%20disponga." target="_blank">Ver más</a></p>
                    </div>
                </div> 
            </section>
        </div>

        <div class="jumbotron text-center bg-transparent mt-5 mb-5">
            <p><a class="btn btn-lg btn-success" href="index.php?r=site/login">Ingresar</a></p>
        </div>

    <?php
    }
    ?>


</div>
