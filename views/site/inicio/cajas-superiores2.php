
              <div class="col-lg-3 col-6" >
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3><?php echo($vacunaciones);?></h3>
                    <p>Vacunaciones</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-medkit"></i>
                  </div>
                  <a href="index.php?r=vacunacion" class="small-box-footer">Ver Vacunaciones <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success" >
                  <div class="inner">
                    <h3><?php echo($veterinarios);?></h3>
                    <p>Veterinarios</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-user-md"></i>
                  </div>
                  <a href="index.php?r=veterinario" class="small-box-footer">Ver Veterinarios <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3><?php echo($usuarios);?></h3>
                    <p>Usuarios</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users"></i>
                  </div>
                  <a href="index.php?r=user" class="small-box-footer">Ver Usuarios <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger" >
                  <div class="inner">
                    <h3><?php echo($produccionlactea);?></h3>
                    <p>Producción Láctea</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-bar-chart"></i>
                  </div>
                  <a href="index.php?r=produccionlactea" class="small-box-footer">Ver Producción <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->