
              <div class="col-lg-3 col-6" >
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3><?php echo($fincas);?></h3>
                    <p>Fincas</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-bag"></i>
                  </div>
                  <a href="index.php?r=finca" class="small-box-footer">Ver Fincas <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success" >
                  <div class="inner">
                    <h3><?php echo($propietarios);?></h3>
                    <p>Propietarios</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person"></i>
                  </div>
                  <a href="index.php?r=propietario" class="small-box-footer">Ver Propietarios <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3><?php echo($criadores);?></h3>
                    <p>Criadores</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person"></i>
                  </div>
                  <a href="index.php?r=criador" class="small-box-footer">Ver Criadores <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger" >
                  <div class="inner">
                    <h3><?php echo($animales);?></h3>
                    <p>Animales</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                  </div>
                  <a href="index.php?r=animal" class="small-box-footer">Ver Animales <i class="ion ion-eye"></i></a>
                </div>
              </div>
              <!-- ./col -->