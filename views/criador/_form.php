<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Criador $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="criador-form">

<?php $form = ActiveForm::begin(); ?>
<table class="table table-hover">
  <tbody>
    <tr>
      <td scope="row">
            <?php echo($form->field($model, 'nombre')->textInput(['rows' => 1])); ?>
      </td>
      <td>
            <?php echo($form->field($model, 'apellido')->textInput(['rows' => 1])); ?>
      </td>
      <td>
            <?php echo($form->field($model, 'cedula')->textInput()); ?>
      </td>
      <td>
            <?php echo($form->field($model, 'telefono')->textInput()); ?>
      </td>
      <td>
            <?php echo($form->field($model, 'correo')->textInput(['rows' => 1])); ?>
      </td>
    </tr>

  </tbody>
</table>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
