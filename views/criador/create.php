<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Criador $model */

$this->title = 'Crear Criador';
$this->params['breadcrumbs'][] = ['label' => 'Criadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="criador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
