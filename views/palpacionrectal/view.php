<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\PalpacionRectal $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Palpaciones Rectales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="palpacion-rectal-view">

    <h1>Palpación Rectal</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'observacion:ntext',
            'fecha',
            //'id_animal',
            //'id_veterinario',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>
    <h4>Animal</h4>
    <?= DetailView::widget([
        'model' => $animal,
        'attributes' => [
            'nombre',
        ],
    ]) ?>
    <h4>Veterinario</h4>
    <?= DetailView::widget([
        'model' => $veterinario,
        'attributes' => [
            'nombre',
            'apellido',
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
