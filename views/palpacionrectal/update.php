<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\PalpacionRectal $model */

$this->title = 'Actualizar Palpación Rectal: ';
$this->params['breadcrumbs'][] = ['label' => 'Palpaciones Rectales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="palpacion-rectal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal,
        'modelVeterinario' => $modelVeterinario,
    ]) ?>

</div>
