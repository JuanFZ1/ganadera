<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\PalpacionRectal $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="palpacion-rectal-form">

    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?= $form->field($model, 'observacion')->textInput(['rows' => 1]) ?>
            </td>
            <td>
                <?= $form->field($model, 'fecha')->textInput() ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'id_animal')->textInput() 
                echo($form->field($model, 'id_animal')->dropDownList(ArrayHelper::map($modelAnimal, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'id_veterinario')->textInput() 

                if(Yii::$app->user->identity->user_type_id == 1){//administrador
                    echo($form->field($model, 'id_veterinario')->dropDownList(ArrayHelper::map($modelVeterinario, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                }else if(Yii::$app->user->identity->user_type_id){// veterinario
                    echo($form->field($model, 'id_veterinario')->dropDownList(ArrayHelper::map($modelVeterinario, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                }else{

                }


                ?>
            </td>
        </tr>
    </tbody>
</table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
