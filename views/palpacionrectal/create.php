<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\PalpacionRectal $model */

$this->title = 'Crear Palpación Rectal';
$this->params['breadcrumbs'][] = ['label' => 'Palpaciones Rectales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="palpacion-rectal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal,
        'modelVeterinario' => $modelVeterinario,
    ]) ?>

</div>
