<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\AnimalSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="animal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'codigo_reg') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'sexo') ?>

    <?= $form->field($model, 'fecha_nacimiento') ?>

    <?php  echo $form->field($model, 'color') ?>

    <?php  echo $form->field($model, 'peso_nacer') ?>

    <?php  echo $form->field($model, 'peso_destete') ?>

    <?php  echo $form->field($model, 'peso_18_meses') ?>

    <?php  echo $form->field($model, 'peso_primer_parto') ?>

    <?php  echo $form->field($model, 'id_finca') ?>

    <?php  echo $form->field($model, 'id_criadores') ?>

    <?php  echo $form->field($model, 'id_grupo_fisiologico') ?>

    <?php  echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
