<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Animal $model */

$this->title = 'Actualizar Animal: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Animales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="animal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelFincas' => $modelFincas,
        'modelCriadores' => $modelCriadores,
        'modelGrupoFisiologicos' => $modelGrupoFisiologicos,
    ]) ?>

</div>
