<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Animal $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="animal-form">

<?php $form = ActiveForm::begin(); ?>
<table class="table table-hover">
  <tbody>
    <tr>
        <td scope="row">
            <?php echo($form->field($model, 'codigo_reg')->textInput(array('maxlength' => true))); ?>
        </td>
        <td>
            <?php echo($form->field($model, 'nombre')->textInput(['maxlength' => true])); ?>
        </td> 
        <td>
            <?php 
            //echo($form->field($model, 'sexo')->textInput(['maxlength' => true]));
            $estadosArray = [ 'Hembra' => 'Hembra', 'Macho' => 'Macho'];
            echo($form->field($model, 'sexo')->dropDownList($estadosArray, ['prompt' => 'Seleccione Uno' ]));
            ?>            
        </td> 
        <td>
            <?php echo($form->field($model, 'fecha_nacimiento')->textInput(['maxlength' => true])); ?>
        </td> 
    </tr>
            
    <tr>
        <td>
            <?php echo($form->field($model, 'color')->textInput(['maxlength' => true])); ?>
        </td> 
        <td>
            <?php 
                echo($form->field($model, 'id_grupo_fisiologico')->dropDownList(ArrayHelper::map($modelGrupoFisiologicos, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td> 
        <td>
        <?php 
            $estadosArray = [ 0 => 'Inactivo', 1 => 'Activo'];
            echo($form->field($model, 'estado')->dropDownList($estadosArray, ['prompt' => 'Seleccione Uno' ]));
        ?>
        </td>
    </tr>
</tbody>
</table>
<h3>Pesos del Animal</h3>
<table>
    <tbody>
    <tr>

        <td>
            <?php echo($form->field($model, 'peso_nacer')->textInput()); ?>
        </td> 
        <td>
            <?php echo($form->field($model, 'peso_destete')->textInput()); ?>
        </td> 
        <td>
            <?php echo($form->field($model, 'peso_18_meses')->textInput()); ?>
        </td> 
        <td>
            <?php echo($form->field($model, 'peso_primer_parto')->textInput()); ?>
        </td>

    </tr>
</tbody>
</table>
<h3>Información Adicional</h3>
<table>
    <tbody>
    <tr>   
        <td>
            <?php 
                //echo($form->field($model, 'id_finca')->textInput()); 
                echo($form->field($model, 'id_finca')->dropDownList(ArrayHelper::map($modelFincas, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td> 
        <td>
            <?php 
                //echo($form->field($model, 'id_criadores')->textInput()); 
                echo($form->field($model, 'id_criadores')->dropDownList(ArrayHelper::map($modelCriadores, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));

            ?>
        </td> 
    </tr>

  </tbody>
</table>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<!-- modelGrupoFisiologicos
modelFincas
modelCriadores -->