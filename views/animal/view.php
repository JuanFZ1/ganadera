<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Animal $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Animales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="animal-view">

    <h1>Animal</h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'codigo_reg',
            'nombre',
            'sexo',
            'fecha_nacimiento',
            'color',
            'peso_nacer',
            'peso_destete',
            'peso_18_meses',
            'peso_primer_parto',
            //'id_finca',
            //'id_criadores',
            //'id_grupo_fisiologico',
            //'estado',
            [
            'label'  => 'Estado',
            'value' => $model->estado == 1  ?  'Activo': 'Inactivo',
            ],
            //'created_at',
            //'updated_at',
        ],
    ]) ?>

    <h4>Finca</h4>
        <?= DetailView::widget([
        'model' => $finca,
        'attributes' => [
            'nombre',
            'direccion',
            'telefono',
        ],
    ]) ?>
    <h4>Criadores</h4>
        <?= DetailView::widget([
        'model' => $criador,
        'attributes' => [
            'nombre',
            'apellido',
            'telefono',
        ],
    ]) ?>
    <h4>Grupo Fisiologico</h4>
        <?= DetailView::widget([
        'model' => $grupo_fisiologico,
        'attributes' => [
            'nombre',
            'sexo',
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
