<?php

use app\models\Animal;
//use app\models\Finca;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\AnimalSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Animales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="animal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Animal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'codigo_reg',
            'nombre',
            'sexo',
            'fecha_nacimiento',
            'color',
            //'peso_nacer',
            //'peso_destete',
            //'peso_18_meses',
            //'peso_primer_parto',
            //'id_finca',
            array('attribute' => 'finca','label' => 'Finca','value' => function($model) { 
                return $model->listFinca->nombre;
            }),
            //'id_criadores',
            array('attribute' => 'criador','label' => 'Criador','value' => function($model) { 
                return $model->listCriador->nombre;
            }),
            //'id_grupo_fisiologico',
            array('attribute' => 'grupo_fisiologico','label' => 'Grupo_Fisiologico','value' => function($model) { 
                return $model->listGrupoFisiologico->nombre;
            }),
            //'estado',
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Animal $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

</div>