<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UserType $model */

$this->title = 'Actualizar Tipo de Usuario: ';
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="user-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
