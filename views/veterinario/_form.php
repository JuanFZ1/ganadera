<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Veterinario $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="veterinario-form">

    <?php $form = ActiveForm::begin(); ?>

    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?php echo($form->field($model, 'codigo_reg')->textarea(['rows' => 1]));?>
            </td>
            <td>
                <?php echo($form->field($model, 'nombre')->textarea(['rows' => 1]));?>
            </td>
            <td>
                <?php echo($form->field($model, 'apellido')->textarea(['rows' => 1]));?>
            </td>
            <td>
                <?php echo($form->field($model, 'cedula')->textInput());?>
            </td>
            <td>
                <?php echo($form->field($model, 'telefono')->textInput());?>
            </td>
            <td>
                <?php echo($form->field($model, 'correo')->textarea(['rows' => 1]));?>
            </td>
        </tr>
    </tbody>
    </table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
