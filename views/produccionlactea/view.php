<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ProduccionLactea $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Producciones Lacteas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="produccion-lactea-view">

    <h1>Produccion Lactea </h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'codigo_reg:ntext',
            'litros',
            'fecha_registro',
            //'id_animal',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>

    <h4>Animal</h4>
        <?= DetailView::widget([
        'model' => $animal,
        'attributes' => [
            'nombre',
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
