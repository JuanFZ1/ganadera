<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProduccionLactea $model */

$this->title = 'Actualizar Producción Láctea: ';
$this->params['breadcrumbs'][] = ['label' => 'Producciones Lacteas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="produccion-lactea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal
    ]) ?>

</div>
