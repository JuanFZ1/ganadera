<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProduccionLactea $model */

$this->title = 'Crear Produccion Lactea';
$this->params['breadcrumbs'][] = ['label' => 'Producciones Lacteas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produccion-lactea-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal
    ]) ?>

</div>
