<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\ProduccionLactea $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="produccion-lactea-form">

    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?= $form->field($model, 'codigo_reg')->textarea(['rows' => 1]) ?>
            </td>
            <td>
                <?= $form->field($model, 'litros')->textInput() ?>
            </td>
            <td>
                <?= $form->field($model, 'fecha_registro')->textInput() ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'id_animal')->textInput() 
                echo($form->field($model, 'id_animal')->dropDownList(ArrayHelper::map($modelAnimal, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                ?>
            </td>
        </tr>
    </tbody>
    </table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
