<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Propietario $model */

$this->title = 'Crear Propietario';
$this->params['breadcrumbs'][] = ['label' => 'Propietarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="propietario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
