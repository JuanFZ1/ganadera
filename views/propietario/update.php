<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Propietario $model */

$this->title = 'Actualizar Propietario: ' . $model->nombre .' '. $model->apellido;
$this->params['breadcrumbs'][] = ['label' => 'Propietarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="propietario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
