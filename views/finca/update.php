<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Finca $model */

$this->title = 'Actualizar Finca: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Fincas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="finca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPropietario' => $modelPropietario,
    ]) ?>

</div>
