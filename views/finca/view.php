<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Finca $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fincas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="finca-view">

    <h1> Finca</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'codigo_reg:ntext',
            'nombre:ntext',
            'direccion:ntext',
            'telefono',
        ],
    ]) ?>
    <h4>Propietario</h4>
        <?= DetailView::widget([
        'model' => $propietario,
        'attributes' => [
            'nombre:ntext',
            'apellido:ntext',
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
