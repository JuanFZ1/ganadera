<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Finca $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="finca-form">
    
<?php $form = ActiveForm::begin(); ?>
<table class="table table-hover">
  <tbody>
    <tr>
      <td scope="row">
            <?php echo ($form->field($model, 'codigo_reg')->textInput()); ?>
      </td>
      <td>
            <?php echo ($form->field($model, 'nombre')->textInput()); ?>
      </td>
      <td>
            <?php echo ($form->field($model, 'direccion')->textInput()); ?>
      </td>
      <td>
            <?php echo ($form->field($model, 'telefono')->textInput()); ?>
      </td>
      <td>
            <?php //echo ($form->field($model, 'id_propietario')->textInput(['rows' => 1])); 
            echo($form->field($model, 'id_propietario')->dropDownList(ArrayHelper::map($modelPropietario, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
    </tr>

  </tbody>
</table>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
