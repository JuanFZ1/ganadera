<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\GrupoFisiologico $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="grupo-fisiologico-form">

    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?= $form->field($model, 'nombre')->textarea(['rows' => 1]) ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'sexo')->textInput() 
                    $estadosArray = [ 'Hembra' => 'Hembra', 'Macho' => 'Macho'];
                    echo($form->field($model, 'sexo')->dropDownList($estadosArray, ['prompt' => 'Seleccione Uno' ]));
                ?>
            </td>
            <td>
                <?= $form->field($model, 'edad')->textInput() ?>
            </td>
        </tr>
        </tbody>
    </table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
