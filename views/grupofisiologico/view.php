<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\GrupoFisiologico $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grupos Fisiologicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="grupo-fisiologico-view">

    <h1>Grupo Fisiológico</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nombre:ntext',
            'sexo',
            'edad',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
