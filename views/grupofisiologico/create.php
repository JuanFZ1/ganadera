<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\GrupoFisiologico $model */

$this->title = 'Crear Grupo Fisiologico';
$this->params['breadcrumbs'][] = ['label' => 'Grupos Fisiologicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-fisiologico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
