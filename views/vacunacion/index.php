<?php

use app\models\Vacunacion;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\VacunacionSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Vacunaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacunacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Vacunación', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'nombre:ntext',
            'principio_activo:ntext',
            'marca:ntext',
            'tipo:ntext',
            'dosis',
            'fecha',
            //'id_animal',
            array('attribute' => 'animal','label' => 'Animal','value' => function($model) { 
                return $model->listAnimal->nombre;
            }),
            //'id_veterinario',
            array('attribute' => 'veterinario','label' => 'Veterinario','value' => function($model) { 
                return $model->listVeterinario->nombre;
            }),
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Vacunacion $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
