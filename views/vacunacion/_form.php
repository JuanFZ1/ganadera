<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Vacunacion $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="vacunacion-form">

    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-hover">
      <tbody>
        <tr>
            <td>
                <?= $form->field($model, 'nombre')->textarea(['rows' => 1]) ?>
            </td>
            <td>
                <?= $form->field($model, 'principio_activo')->textarea(['rows' => 1]) ?>
            </td>
            <td>
                <?= $form->field($model, 'marca')->textarea(['rows' => 1]) ?>
            </td>
            <td>
                <?= $form->field($model, 'tipo')->textarea(['rows' => 1]) ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $form->field($model, 'dosis')->textInput(['maxlength' => true]) ?>
            </td>
            <td>
                
                <?php echo($form->field($model, 'fecha')->textInput());?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'id_animal')->textInput() 
                echo($form->field($model, 'id_animal')->dropDownList(ArrayHelper::map($modelAnimal, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
                ?>
            </td>
            <td>
                <?php 
                //$form->field($model, 'id_veterinario')->textInput() 
                echo($form->field($model, 'id_veterinario')->dropDownList(ArrayHelper::map($modelVeterinario, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));

                ?>
            </td>
        </tr>
    </tbody>
</table>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
