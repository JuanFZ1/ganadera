<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Vacunacion $model */

$this->title = 'Crear Vacunación';
$this->params['breadcrumbs'][] = ['label' => 'Vacunaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacunacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelAnimal' => $modelAnimal,
        'modelVeterinario' => $modelVeterinario,

    ]) ?>

</div>
