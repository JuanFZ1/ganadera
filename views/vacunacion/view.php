<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Vacunacion $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vacunaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vacunacion-view">

    <h1>Vacunación</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'nombre:ntext',
            'principio_activo:ntext',
            'marca:ntext',
            'tipo:ntext',
            'dosis',
            'fecha',
            //'id_animal',
            //'id_veterinario',

        ],
    ]) ?>
    <h4>Animal</h4>
    <?= DetailView::widget([
        'model' => $animal,
        'attributes' => [
            'nombre:ntext',
        ],
    ]) ?>
    <h4>Veterinario</h4>
    <?= DetailView::widget([
        'model' => $veterinario,
        'attributes' => [
            'nombre',
            'apellido'
        ],
    ]) ?>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
