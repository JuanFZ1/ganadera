<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ArbolGenealogico $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Arboles Genealogicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="arbol-genealogico-view">

    <h1>Arbol Genealógico</h1>

    <table class="table table-hover"> 
        <tr>
            <td>
                <h4>Abuelo Paterno</h4>
                <?= DetailView::widget([
                    'model' => $modelabuelo_paterno,
                    'attributes' => [
                        'nombre',
                    ],
                ]) ?>
            </td>
            <td>
                <h4>Abuela Paterna</h4>
                <?= DetailView::widget([
                    'model' => $modelabuela_paterna,
                    'attributes' => [
                        'nombre',
                    ],
                ]) ?>
            </td>
            <td>
                <h4>Abuelo Materno</h4>
                <?= DetailView::widget([
                    'model' => $modelabuelo_materna,
                    'attributes' => [
                        'nombre',
                    ],
                ]) ?>
            </td>
            <td>
                <h4>Abuela Materna</h4>
                <?= DetailView::widget([
                    'model' => $modelabuela_materna,
                    'attributes' => [
                        'nombre',
                    ],
                ]) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4>Padre</h4>
                    <?= DetailView::widget([
                        'model' => $modelpadre,
                        'attributes' => [
                            'nombre',
                        ],
                    ]) ?>
            </td>
            <td colspan="2">
                <h4>Madre</h4>
                <?= DetailView::widget([
                    'model' => $modelmadre,
                    'attributes' => [
                        'nombre',
                    ],
                ]) ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h4>Animal</h4>
                    <?= DetailView::widget([
                        'model' => $modelanimal,
                        'attributes' => [
                            'nombre',
                        ],
                    ]) ?>
            </td>
        </tr>
    </table>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas Seguro de querer Eliminar este Registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
