<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\ArbolGenealogico $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="arbol-genealogico-form">

    <?php $form = ActiveForm::begin(); ?>
<table class="table table-hover">
  <tbody>
    <tr>
        <td>
            <?= $form->field($model, 'codigo_reg')->textInput() ?>
        </td>
        <td>
            <?php 
                //$form->field($model, 'id_animal')->textInput()
                    echo($form->field($model, 'id_animal')->dropDownList(ArrayHelper::map($modelanimal, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ])); 
            ?>
        </td>
    </tr>
</tbody>
</table>
    <h3>Padres</h3>
<table>
<tbody>
    <tr>
        <td>
            <?php 
                //$form->field($model, 'id_padre')->textInput() 
                echo($form->field($model, 'id_padre')->dropDownList(ArrayHelper::map($modelpadre, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
        <td>
            <?php 
                //$form->field($model, 'id_madre')->textInput() 
                echo($form->field($model, 'id_madre')->dropDownList(ArrayHelper::map($modelmadre, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
    </tr>
</tbody>
</table>
    <h3>Abuelos</h3>
<table>
<tbody>
    <tr>
        <td>
            <?php 
                //$form->field($model, 'id_abuelo_paterno')->textInput() 
                echo($form->field($model, 'id_abuelo_paterno')->dropDownList(ArrayHelper::map($modelabuelo_paterno, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
        <td>
            <?php 
                //$form->field($model, 'id_abuela_paterna')->textInput() 
                echo($form->field($model, 'id_abuela_paterna')->dropDownList(ArrayHelper::map($modelabuela_paterna, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
        <td>
            <?php 
                //$form->field($model, 'id_abuelo_materna')->textInput() 
                echo($form->field($model, 'id_abuelo_materna')->dropDownList(ArrayHelper::map($modelabuelo_materna, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>
        <td>
            <?php 
                //$form->field($model, 'id_abuela_materna')->textInput() 
                echo($form->field($model, 'id_abuela_materna')->dropDownList(ArrayHelper::map($modelabuela_materna, 'id', 'nombre'), ['prompt' => 'Seleccione Uno' ]));
            ?>
        </td>

        
    </tr>
    </tbody>
</table>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
