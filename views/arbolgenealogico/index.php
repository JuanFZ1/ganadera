<?php

use app\models\ArbolGenealogico;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ArbolGenealogicoSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Arbol Genealógicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arbol-genealogico-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Arbol Genealógico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'codigo_reg:ntext',
            //'id_animal',
            array('attribute' => 'animal','label' => 'Animal','value' => function($model) { 
                return $model->listAnimal->nombre;
            }),
            //'id_padre',
            array('attribute' => 'padre','label' => 'Padre','value' => function($model) { 
                return $model->listPadre->nombre;
            }),
            //'id_madre',
            array('attribute' => 'madre','label' => 'Madre','value' => function($model) { 
                return $model->listMadre->nombre;
            }),
            //'id_abuelo_paterno',
            array('attribute' => 'abuelo_paterno','label' => 'Abuelo Paterno','value' => function($model) { 
                return $model->listAbuelopaterno->nombre;
            }),
            //'id_abuela_paterna',
            array('attribute' => 'abuela_paterna','label' => 'Abuela Paterna','value' => function($model) { 
                return $model->listAbuelaPaterna->nombre;
            }),
            //'id_abuelo_materna',
            array('attribute' => 'abuelo_materna','label' => 'Abuelo Materno','value' => function($model) { 
                return $model->listAbueloMaterno->nombre;
            }),
            //'id_abuela_materna',
            array('attribute' => 'abuela_materna','label' => 'Abuela Materna','value' => function($model) { 
                return $model->listAbuelaMaterna->nombre;
            }),
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ArbolGenealogico $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
