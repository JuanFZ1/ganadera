<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ArbolGenealogicoSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="arbol-genealogico-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'codigo_reg') ?>

    <?= $form->field($model, 'id_animal') ?>

    <?= $form->field($model, 'id_padre') ?>

    <?= $form->field($model, 'id_madre') ?>

    <?php  echo $form->field($model, 'id_abuelo_paterno') ?>

    <?php  echo $form->field($model, 'id_abuela_paterna') ?>

    <?php  echo $form->field($model, 'id_abuelo_materna') ?>

    <?php  echo $form->field($model, 'id_abuela_materna') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
