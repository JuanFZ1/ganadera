<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ArbolGenealogico $model */

$this->title = 'Crear Arbol Genealógico';
$this->params['breadcrumbs'][] = ['label' => 'Arboles Genealogicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arbol-genealogico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelanimal' => $modelanimal,
        'modelpadre' => $modelpadre,
        'modelmadre' => $modelmadre,
        'modelabuelo_paterno' => $modelabuelo_paterno,
        'modelabuela_paterna' => $modelabuela_paterna,
        'modelabuelo_materna' => $modelabuelo_materna,
        'modelabuela_materna' => $modelabuela_materna,
    ]) ?>

</div>
